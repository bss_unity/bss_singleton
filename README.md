Unity에서 싱글톤 패턴을 적용하기 위한 프로젝트입니다.

## FEATURES 
- Weak Singleton 
- Unique Singleton
- Scriptable Singleton

### CREATE BY
- Sanghun Lee (Unity Client Developer)
- Email: tkdgns1284@gmail.com
- Gitlab: https://gitlab.com/tkdgns1284